<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'phenix-db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'rXrEyV:.:%UJ|S$*w2ZYKhpY[Vlk@2>v{tO-L-<.rEuR}0AoqL^Eu%boW^VL<:%h' );
define( 'SECURE_AUTH_KEY',  '6e`.mzAajTcAwzMi0ssL+IX{TaTz(6dN4TXCWn@ET+CWlORH;UmaT`l:x0OJityN' );
define( 'LOGGED_IN_KEY',    'SA4{O9<29@F6,|/U0zp>+f`nA}3+3vjcs/:woCJ%x%n/IuO5K^T#p$8.sZ>a)M-?' );
define( 'NONCE_KEY',        ' z6sh3d/ <S<KH{)[#M*[LR#J[L+AGvOWoYDqV!?7&<Epq]~^6N2+jIutoa;q0(M' );
define( 'AUTH_SALT',        'J7[lJ<_Yjl)3p73GC,#x2U^r59g0hjFh6}U{emR)rF.hEe6X9G44>B@mp|~tw4Jt' );
define( 'SECURE_AUTH_SALT', 'CZ$0/sJp9HE#+Zq:3;$S72){26FI{iE3pG9m#=O3T98t[iR{St_^:i)J{+nQ|!( ' );
define( 'LOGGED_IN_SALT',   '{7^EpQ[sC;6;rs4A_3;2$A|1_9 _bo-Jx$7B*<@3p&<3&Wh:l.MS)[WNY+O%t>mp' );
define( 'NONCE_SALT',       '!7Uk-h|Pb8dAL/)i.t1]h18?GN^?//s(A{U<!Q%:|b%I),X?(MjmXQOUySiC@BiC' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
